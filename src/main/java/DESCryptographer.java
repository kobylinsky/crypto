import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

public class DESCryptographer extends Cryptographer {

    private static final String DES_CBC_ALGORITHM = "DES/CBC/PKCS5Padding";

    private static final byte[] INIT_VECTOR = new byte[]{0x10, 0x10, 0x01, 0x04, 0x01, 0x01, 0x01, 0x02};

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public byte[] decrypt(byte[] encryptedText, byte[] key) throws Exception {
        System.out.println("Encrypted text: " + new String(encryptedText));

        SecretKey keySecret = new SecretKeySpec(key, "DES");
        System.out.println("Key: " + new String(key));

        Cipher cipher = Cipher.getInstance(DES_CBC_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, keySecret, new IvParameterSpec(INIT_VECTOR));
        byte[] decryptedText = cipher.doFinal(encryptedText);
        System.out.println("Decrypted text: " + new String(decryptedText));

        return decryptedText;
    }

    public byte[] encrypt(byte[] decryptedText, byte[] key) throws Exception {
        if (decryptedText == null || decryptedText.length == 0) {
            throw new NullPointerException("Please, specify text for encryption");
        }
        if (key == null || key.length == 0) {
            throw new NullPointerException("Please, specify key for encryption");
        }

        System.out.println("Clear text: " + new String(decryptedText));

        SecretKey keySecret = new SecretKeySpec(key, "DES");
        System.out.println("Key: " + new String(key));

        Cipher cipher = Cipher.getInstance(DES_CBC_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, keySecret, new IvParameterSpec(INIT_VECTOR));
        byte[] encryptedText = cipher.doFinal(decryptedText);
        System.out.println("Encrypted text: " + new String(encryptedText));

        return encryptedText;
    }

    public byte[] generateKey() throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance("DES").generateKey().getEncoded();
    }

}
