import java.security.NoSuchAlgorithmException;
import java.util.Random;

public abstract class Cryptographer {

    abstract byte[] decrypt(byte[] encryptedText, byte[] key) throws Exception;

    abstract byte[] encrypt(byte[] decryptedText, byte[] key) throws Exception;

    static byte[] hexStringToByteArray(String hexString) {
        int stringLength = hexString.length();
        byte[] outputBytes = new byte[stringLength / 2];
        for (int i = 0; i < stringLength; i += 2) {
            outputBytes[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return outputBytes;
    }

    static String byteArrayToHexString(byte[] bytes) {
        StringBuilder hexRepresentation = new StringBuilder();
        for (byte b : bytes) {
            hexRepresentation.append(String.format("%02X", b));
        }
        return hexRepresentation.toString();
    }

    byte[] generateKey() throws NoSuchAlgorithmException {
        char[] chars = "abcdefghijklmnopqrstuvwxyz0123456789_-+=?!".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 16; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString().getBytes();
    }



}
