import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

public class TestIDEA {

    private ErrorCollector collector = new ErrorCollector();

    @Test
    public void testEncDecr() throws Exception {
        Cryptographer cryptographer = new IDEACryptographer();

        String toEncrypt = "37894y8w3tnf893t589nfw35tn89w";
        byte[] key = cryptographer.generateKey();

        String encrypted = new String(cryptographer.encrypt(toEncrypt.getBytes(), key));
        String decrypted = new String(cryptographer.decrypt(encrypted.getBytes(), key));

        collector.checkThat("Encryption failed", decrypted, CoreMatchers.equalTo(toEncrypt));
    }
}
